# Springboot - Training - Day 1

## spb-training-excercise-day1
--------------------------------------------------------------------

### Steps

1. Create new Springboot Project with the following details
    a. Java 11 and above
    b. War package
    c. Springboot parent version 2.7.7
    d. Add dependency - Springboot Web, Lombok, tomcat-jasper, jstl
2. Create the following file(s)
    a. application.properties
    b. Controller (Index and Employee)
    c. JSP files - Create the following files under src/main/webapp
        1. Create folder - webapp/WEB-INF/views
        2. Create JSP files under WEB-INF/views - index.jsp and employee.jsp
