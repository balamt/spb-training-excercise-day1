package in.bala.employee.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController implements ErrorController {
	
	private static final String PATH = "/error";

	@GetMapping("/index")
	public String viewIndex() {
		return "index";
	}
	
	@GetMapping(PATH)
	public String getErrorResponse() {
		return "Requested resource not found. Please Goto <a href=\"index\">Home</a>";
	}
	
	public String getErrorPath() {
		return PATH;
	}

}
