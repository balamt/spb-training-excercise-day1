package in.bala.employee.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import in.bala.employee.model.Employee;

@Controller
@RequestMapping("/employee")
public class EmployeeController {
	
	@Value("${message}")
	String message;

	@GetMapping
	public String viewEmployee(@RequestParam String id, ModelMap map) {
		map.addAttribute("message", message);
		map.addAttribute("employee", new Employee(Long.parseLong(id), "Bob", 34, 2350.12));
		return "employee";
	}

}
