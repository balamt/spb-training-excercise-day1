package in.bala.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeJspSvcAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeJspSvcAppApplication.class, args);
	}

}
