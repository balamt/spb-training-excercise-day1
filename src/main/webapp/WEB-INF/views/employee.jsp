<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Employee Details for ${ employee.id }</title>
</head>
<body>
<h1>${message}</h1>
<h3>Employee Details for ${ employee.id }</h3>
<p>
Name : ${employee.name }</br>
Age : ${employee.age } </br>
Salary: ${employee.salary }
</p>
</body>
</html>